import numpy as np
from stage import generate_board, print_board


class ANN:

    def __init__(self, board_side_length=8, hidden_layer_nodes=None):
        # Number of cells on each side of the square board
        self.board_side_length = board_side_length

        # Total number of cells
        n_cells = self.board_side_length**2

        # If using the default hidden_layer_nodes
        if hidden_layer_nodes is None:
            hidden_layer_nodes = [n_cells]

        # Put together the list giving the number of nodes in each layer
        # The input and output layers both correspond to the board, having n_cells nodes
        nodes_per_layer = [n_cells] + hidden_layer_nodes + [n_cells]

        # Initialize the map between each adjacent pair of layers, along with the bias
        # Each map is an m x n matrix where m is number of nodes on the on the input side and n is the number of outputs
        # Each map has an associated scalar bias, which shifts the kernel of the map
        # For each map and bias, start with random numbers distributed uniformly on the interval [-1, 1]
        self.bias = []
        self.map = []
        for i in range(len(nodes_per_layer) - 1):
            self.bias.append(2 * np.random.rand() - 1)
            self.map.append(2 * np.random.rand(nodes_per_layer[i + 1], nodes_per_layer[i]) - 1)

    def activation(self, x):
        y = np.tanh(x)
        # y = np.maximum(0, x)

        return y

    def run_game(self, ships=None, key=None):
        # Total number of cells on the square board
        n_cells = self.board_side_length**2

        # Initialize the playing board to zeros, meaning each cell is unexplored
        board = np.zeros(n_cells, np.int)

        # Create a list of the indices of the unexplored cells
        idx_list = range(n_cells)

        # Length of each ship
        if ships is None:
            ships = [5, 4, 3, 3, 2]

        # Generate an answer key board of size n x n and populate it randomly with ships
        if key is None:
            key = generate_board(self.board_side_length, ships)

        # print "MAP:"
        # for m in self.map:
        #     print m
        # print "###############################\n"
        #
        # print "KEY:"
        # print_board(key)
        # print "###############################\n"
        #
        # print "Bias:"
        # for i in range(len(self.map)):
        #     print self.bias[i]
        # print "###############################\n"
        #
        # iter = 0
        # print "x " + str(iter)
        # print_board(x)
        # print "###############################\n"

        # Initialize the turn and hit counters
        n_turns = 0
        n_hits = 0

        # While there is still a cell and ship left to hit
        while n_turns < n_cells and n_hits < sum(ships):
            # Make a copy of the current board for the input layer
            x = board

            # Iterate through each layer
            for i in range(len(self.map)):
                # Apply the map as a matrix dot product on the input x
                # Shift the resulting vector by the scalar bias
                # Pass the biased vector through the activation function, returning a value on the interval (-1, 1)
                # Save the output as x and pass it as the input to the next map
                x = self.activation(self.map[i].dot(x) + self.bias[i])

            # Now that x represents the data on the output layer, use it to pick the next cell to attack
            # We chose to attack the cell with the largest x value that has not yet been attacked
            # Initialize the lower bound
            best_choice_value = -1.0
            best_choice_idx = -1
            for idx in idx_list:
                if best_choice_value <= x[idx]:
                    best_choice_value = x[idx]
                    best_choice_idx = idx

            # Attack the chosen cell and check against the key
            board[best_choice_idx] = key[best_choice_idx]

            # Remove that index from the list of unexplored cells
            idx_list.remove(best_choice_idx)

            # iter += 1
            # print "x " + str(iter) + ": Attack (" + str(best_choice_idx) + ")"
            # print_board(x)
            # print "###############################\n"

            # If we got a hit, increment the counter
            if key[best_choice_idx] == 1:
                n_hits += 1

            # Increment the turn counter
            n_turns += 1

        # How many turns did it take to win the game?
        return n_turns

    def copy(self, parent):
        # Copy the map and bias
        self.bias = np.copy(parent.bias)
        self.map = np.copy(parent.map)

    def mutate(self, mutation_sd):
        # Randomly perturb each map and bias value by throwing a gaussian of width mutation_sd about the current value
        for i in range(len(self.map)):
            p_mutate = 1.0 / self.map[i].size
            bias_mask = np.random.binomial(1, p_mutate)
            map_mask = np.random.binomial(1, p_mutate, self.map[i].shape)
            self.bias[i] += bias_mask * np.random.normal(0, mutation_sd)
            self.map[i] += map_mask * np.random.normal(0, mutation_sd, self.map[i].shape)
