import numpy as np


# 2D super index
def idx2(i, j, n):
    return i + n * j


def generate_board_stats(n, ships):
    ship_sum = np.zeros(n**2, np.int)
    ship_isolate_sum = [np.zeros(n**2, np.int) for s in range(len(ships))]

    itmax = 10**8
    it = 0
    while it < itmax:

        key = np.zeros(n**2, np.int)

        ship_isolate = [np.zeros(n**2, np.int) for s in range(len(ships))]

        for s in range(len(ships)):
            p = np.random.randint(0, 2)

            x = np.random.randint(0, n - (ships[s] - 1) * (1 - p))
            y = np.random.randint(0, n - (ships[s] - 1) * p)

            for i in range(ships[s]):
                idx = idx2(x + i * (1 - p), y + i * p, n)
                key[idx] += 1
                ship_isolate[s][idx] += 1

        if (not any(v > 1 for v in key)) and sum(key) == sum(ships):
            if it % (itmax / 100) == 0:
                print it

            it += 1
            ship_sum += key
            for s in range(len(ships)):
                ship_isolate_sum[s] += ship_isolate[s]

    with open("output/board_stats_" + str(n**2) + ".txt", "w") as output:
        output.write("# total boards = " + str(it) + "\n")

        for j in range(n):
            for i in range(n):
                idx = idx2(i, j, n)
                output.write(str(i) + " " + str(j) + " " + str(ship_sum[idx]) + " ")

                for s in ship_isolate_sum:
                    output.write(str(s[idx]) + " ")
                output.write("\n")
            output.write("\n")


def generate_board(n, ships):
    valid = False
    while not valid:
        key = np.zeros(n**2, np.int)

        for ship in ships:
            p = np.random.randint(0, 2)

            x = np.random.randint(0, n - (ship - 1) * (1 - p))
            y = np.random.randint(0, n - (ship - 1) * p)

            for i in range(ship):
                key[idx2(x + i * (1 - p), y + i * p, n)] += 1

        if (not any(v > 1 for v in key)) and sum(key) == sum(ships):
            valid = True

    return 2 * key - np.ones(n**2, np.int)


def print_board(board):
    n = int(np.sqrt(len(board)))
    b = np.zeros((n, n), np.int)
    for j in range(n):
        for i in range(n):
            b[i][j] = board[idx2(i, j, n)]
            if int(board[idx2(i, j, n)]) == 1:
                b[i][j] = 1
            elif int(board[idx2(i, j, n)]) == -1:
                b[i][j] = 0
            # else:
            #     b[i][j] = 0

    print sum(sum(b))
    print b
