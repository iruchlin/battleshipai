import threading
import numpy as np
from ann import ANN
from stage import generate_board
import multiprocessing

# Number of parents to select from the population
n_parents = 4

# Number of children from each selected parent
n_children = 8

# Total population size for each generation
n_players = n_parents * n_children

# Initialize players with an ANN and an average score
ann_list = []
for i in range(n_players):
    # ann_list.append([ANN(), 0.0])
    ann_list.append([ANN(hidden_layer_nodes=[64]), 0.0])

# Total number of generations
n_gens = 10**6

# Number of games for each player
games_per_gen = 1

key = generate_board(8, [5, 4, 3, 3, 2])

for gen in range(n_gens):

    for a in ann_list:
        # Initialize the average turn count
        a[1] = 0.0
        for game in range(games_per_gen):
            # Sum the number turns to win each game
            a[1] += a[0].run_game()
            # a[1] += a[0].run_game(key=key)
        # Average number of turns per game
        a[1] /= games_per_gen

    # Sort players by fewest number of turns on average
    ann_list = sorted(ann_list, key=lambda a: a[1])

    # Lucky you
    rand_ann = np.random.randint(n_parents - 1, n_players)
    ann_list[n_parents - 1][0].copy(ann_list[rand_ann][0])
    ann_list[n_parents - 1][1] = ann_list[rand_ann][1]

    # Print the scores for the winners
    print "Gen " + str(gen) + " |",
    for i in range(n_parents):
        print "  " + str(ann_list[i][1]),
    print ""

    # Overwrite the losers with copies of the winners
    for p in range(n_parents):
        for c in range(1, n_children):
            ann_list[p + n_parents * c][0].copy(ann_list[p][0])

    # Set the width of the mutation according to a cooling schedule
    # mutation_sd = np.exp(-gen / 10.0**5)
    mutation_sd = 0.01
    # Elitist mutation
    for a in range(n_parents, n_players):
        # Mutate each player for the next generation
        ann_list[a][0].mutate(mutation_sd)
