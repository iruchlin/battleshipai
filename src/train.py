import numpy as np
import matplotlib.pyplot as plt
from collections import deque
from battleship import Battleship
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten
from keras.regularizers import l1
from keras.optimizers import sgd


def play_by_play(turn_, state_, action_, reward_, reward_sum_):
    print "turn = " + str(turn_)
    print "reward_sum = " + str(reward_sum_)
    print state_
    print "action = " + str(action_)
    print "reward = " + str(reward_)
    print "============================="


# board_shape = [10, 10]
# ships = [5, 4, 3, 3, 2]

board_shape = [4, 4]
ships = [3, 2]

bs = Battleship(board_shape=board_shape, ships=ships)

n_episodes = 10**4
# n_episodes = 1
discount = 0.95
reg_rate = 0.01
eps_action = 0.5
decay_factor = 0.998
learning_rate = 0.2
batch_size = 32
memory_size = 128

input_shape = [1] + board_shape

# Construct the sequential model
model = Sequential()

model.add(Conv2D(16, (2, 2), input_shape=input_shape, activation='relu', padding='same',
                 bias_initializer='random_uniform', data_format='channels_first',
                 kernel_regularizer=l1(reg_rate), bias_regularizer=l1(reg_rate)))
model.add(Conv2D(16, (2, 2), activation='relu', padding='same',
                 kernel_regularizer=l1(reg_rate), bias_regularizer=l1(reg_rate)))
model.add(Flatten())
model.add(Dense(64, activation='relu', kernel_regularizer=l1(reg_rate), bias_regularizer=l1(reg_rate)))
model.add(Dense(bs.n_cells, activation='linear', kernel_regularizer=l1(reg_rate), bias_regularizer=l1(reg_rate)))

# model.add(Dense(64, input_shape=input_shape, activation='relu', bias_initializer='random_uniform',
#                 kernel_regularizer=l1(reg_rate), bias_regularizer=l1(reg_rate)))
# model.add(Flatten())
# model.add(Dense(64, activation='relu', kernel_regularizer=l1(reg_rate), bias_regularizer=l1(reg_rate)))
# model.add(Dense(bs.n_cells, kernel_regularizer=l1(reg_rate), bias_regularizer=l1(reg_rate)))

model.compile(loss='mean_squared_error', optimizer=sgd(lr=learning_rate))
model.summary()

# Initialize replay memory queue
memory = deque(maxlen=memory_size)

# Pretrain the memory bank
# Before training the network, initialize the memory with random actions
print "Pretraining..."
for episode in range(memory_size):
    # Reset the player and generate a new key
    bs.reset()

    # Play game until every cell of every ship has been hit
    while not bs.game_over:
        memory.append(bs.step())

# Initialize training metrics
reward_list = []
turn_list = []
loss_list = []

# Each full play through of a game is one episode
# Train the network to predict Q values for each action on a given state by playing many games and tracking the
# resulting rewards.
print "Training..."
for episode in range(n_episodes):
    # Training progress indicator
    # if i % (n_episodes / 100) == 0:
    #     print("Episode {} of {}".format(episode, n_episodes))

    # Reset the player and generate a new key
    bs.reset()

    # Threshold for taking a random action, decays exponentially
    eps_action *= decay_factor

    # Initialize total reward accumulated in this game
    reward_sum = 0
    loss = 0.0

    # Play game until every cell of every ship has been hit
    while not bs.game_over:
        # Play one turn of the current game
        # The resulting (state, action, reward, new_state, game_over) is appended to the memory queue
        memory.append(bs.step(model=model, eps_action=eps_action))

        # play_by_play(bs.n_turns, s, a, r, reward_sum)

        # Train on a batch of memories selected randomly from the memory
        memory_idx = np.random.choice(memory_size, size=batch_size, replace=False)

        # Split the batch into individual batches for state, action, reward, and new state
        s_batch = np.array([[memory[i][0]] for i in memory_idx])  # (batch_size, n_channels=1, board_size[0], board_size[1])
        a_batch = np.array([memory[i][1] for i in memory_idx])  # (batch_size, 1)
        r_batch = np.array([memory[i][2] for i in memory_idx])  # (batch_size, 1)
        ns_batch = np.array([[memory[i][3]] for i in memory_idx])  # (batch_size, n_channels=1, board_size[0], board_size[1])
        go_batch = np.array([memory[i][4] for i in memory_idx])  # (batch_size, 1)

        # Predict the Q values for this batch of states
        target_vec = model.predict(s_batch)  # (batch_size, bs.n_cells)

        # Approximate the Q value for this action
        # The Q value includes a discounted prediction for best Q value in the next state
        target = r_batch + discount * np.max(model.predict(ns_batch), axis=1) * go_batch  # (batch_size, 1)

        # Adjust the target Q value for this action
        for j in range(batch_size):
            target_vec[j, a_batch[j]] = target[j]

        # Train the model on the batch
        # model.fit(np.array(s_batch), target_vec, epochs=1, verbose=0)
        loss += model.train_on_batch(s_batch, target_vec)

        # Sum the reward from each action
        reward_sum += memory[-1][2]

    print("Episode {:d}/{:d} | Loss {:.3e} | Reward {:d} | Turns {:d} | Misses {:d} | Repeats {:d}".format(episode, n_episodes, loss, reward_sum, bs.n_turns, bs.n_misses, bs.n_repeats))

    reward_list.append(reward_sum)
    loss_list.append(loss)

    # Number of turns to finish game
    turn_list.append(bs.n_turns)

# plt.plot(turn_list)
plt.plot(reward_list)
plt.show()
