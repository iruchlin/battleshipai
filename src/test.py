# import errno
# import os
# import time
import numpy as np
import matplotlib.pyplot as plt
# from ann import ANN
# from stage import generate_board, print_board, idx2, generate_board_stats

from battleship import Battleship

bs = Battleship(board_shape=(4, 4), ships=[3, 2])
# bs = Battleship(board_shape=(8, 8), ships=[5, 4, 3, 3, 2])

key_list = set()
index_list = set()

n_unique_boards = []
n_boards = 10000
n_turns = np.zeros(n_boards)
for i in range(n_boards):
    if i % (n_boards / 100) == 0:
        print i
    bs.reset()
    key = bs.generate_key(return_key=True).flatten()
    key_list.add(hash(tuple(key)))
    n_unique_boards.append(len(key_list))

    while not bs.game_over:
        bs.step()
        n_turns[i] += 1.0

print len(key_list)
print bs.generate_key(return_key=True)

plt.plot(n_unique_boards, '-')
plt.show()

print np.mean(n_turns)

# If output directory doesn't exist, create it quietly
# out_dir = "output"
#
# try:
#     os.makedirs(out_dir)
# except OSError as exc:  # Python >2.5
#     if exc.errno == errno.EEXIST and os.path.isdir(out_dir):
#         pass
#     else:
#         raise

# Print iterations progress

# A = np.array([[1, 2, 3],
#               [3, 4, 5]])
# x = np.array([1, 2, 3])
# y = np.array([1, 2])
# b = A.dot(x)
# c = y.dot(A)

#print A

#print x

#print y

#print b

#print c

#print np.tanh(c)

#print np.__version__

# print np.zeros((8, 4), np.int)

#print -np.ones(64)

# print np.random.rand(3, 4)

#print np.random.normal(1,0.1,(3,3))

#print np.random.normal(A, 0.01, A.shape)

#print np.zeros((3,3))+1

#print np.zeros((3,3)).astype(int)

# a = ANN()
#
# print "n_turns = " + str(a.run_game())
#
# start1 = time.time()
#
# # with open("output/average_turns_per_ann_2hidden.txt", "w") as output:
#

# generate_board_stats(10, [5, 4, 3, 3, 2])
# print generate_board(8, [5, 4, 3, 3, 2])
# nplayers = 10**4
# ngames = 10**4
#
# # avg_list = []
#
# hist_list = np.zeros(64+1)
#
# for i in range(nplayers):
#     b = ANN()
#     # key = np.zeros(b.board_side_length**2)
#
#     avg = 0.0
#     print "Running ANN #" + str(i)
#
#     for j in range(ngames):
#         nturns = b.run_game()
#         hist_list[nturns] += 1
#         avg += nturns
#         # key += generate_board(b.board_side_length, [5, 4, 3, 3, 2])
#
#     avg /= ngames
#
#     # avg_list.append(avg)
#
#
#         # output.write(str(avg / n) + "\n")
#
# print hist_list
#
# print avg
#
# with open("output/hist_list_1e8.txt", "w") as output:
#     for i in range(len(hist_list)):
#         output.write(str(i) + " " + str(hist_list[i]) + "\n")

# stop1 = time.time()
# print "Completed in " + str(round(stop1 - start1, 2)) + " seconds"

# plt.plot(hist_list)
# plt.show()

# plt.hist(key, density=True, stacked=True)
# plt.show()


# for i in range(1e6):
#     generate_board(8, [5, 4, 3, 3, 2])
# n = 10
# trials = 10**6
# board = np.zeros(n**2)
# for i in range(trials):
#     board += 0.5 * (generate_board(n, [5, 4, 3, 3, 2]) + 1)
# board /= trials
#
# ans = np.zeros((n, n))
# for i in range(n):
#     for j in range(n):
#         ans[i][j] = board[idx2(i, j, n)]
#
# stop1 = time.time()
# print "Completed in " + str(round(stop1 - start1, 2)) + " seconds"
#
# print(ans)
#
# plt.imshow(ans, cmap='hot', interpolation='nearest')
# plt.show()
