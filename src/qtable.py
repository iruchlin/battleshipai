import numpy as np
from battleship import Battleship
from matplotlib import pyplot as plt

np.random.seed(1234)

board_shape = [4, 4]
ships = [3, 2]

bs = Battleship(board_shape=board_shape, ships=ships)

board_size = board_shape[0] * board_shape[1]

qtable = np.zeros((3**board_size, board_size))

n_episodes = 4 * 10**5
learning_rate = 0.9
discount = 0.95

turn_list = np.zeros(n_episodes)
reward_list = np.zeros(n_episodes)

for episode in range(n_episodes):
    bs.reset()

    n_turns = 0
    reward = 0

    while not bs.game_over:
        s, a, r, ns, go = bs.step(model=qtable, use_qtable=True)

        flat_state = np.nonzero((s + 1).flatten())[0]
        board_idx = np.sum(3 ** flat_state * (s + 1).flatten()[flat_state])

        ns_flat_state = np.nonzero((ns + 1).flatten())[0]
        ns_board_idx = np.sum(3 ** ns_flat_state * (ns + 1).flatten()[ns_flat_state])

        qtable[board_idx, a] += learning_rate * (r + discount * np.max(qtable[ns_board_idx]) - qtable[board_idx, a])

        n_turns += 1
        reward += r

    turn_list[episode] = n_turns
    reward_list[episode] = reward

    if episode % (n_episodes // 100) == 0:
        print('Episode {:d}/{:d} | Reward {:d} | Turns {:d}'.format(episode, n_episodes, reward, n_turns))

plt.plot(reward_list, '.', alpha=0.01)
plt.show()

plt.plot(turn_list, '.', alpha=0.01)
plt.show()
