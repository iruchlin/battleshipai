import numpy as np


class Battleship:

    def __init__(self, board_shape=None, ships=None):
        """Default initializer for the Battleship game.

        Parameters
        ----------
        board_shape : [int, int]
            Shape of the 2D game grid.
        ships : list of ints
            List of ship lengths.

        Returns
        -------
        :obj:
            New initialized Battleship player object.
        """
        # Default 8x8 grid
        if board_shape is None:
            self.board_shape = [8, 8]
        else:
            self.board_shape = board_shape

        # Default ship set
        if ships is None:
            self.ships = [5, 4, 3, 3, 2]
        else:
            self.ships = ships

        # Total number of cells on the board grid
        self.n_cells = int(np.prod(self.board_shape))

        # Total number of ship cells
        self.max_hits = np.sum(self.ships)

        # Initialize gameplay variables
        self.n_hits = 0
        self.n_misses = 0
        self.n_repeats = 0
        self.n_turns = 0
        self.game_over = False

        # Empty key and state
        self.key = np.zeros(self.board_shape, dtype=np.int32)
        self.state = np.zeros(self.board_shape, dtype=np.int32)

        # Map 1D action_map to 2D (i, j)
        self.action_map = []
        for j in range(self.board_shape[1]):
            for i in range(self.board_shape[0]):
                self.action_map.append((i, j))

    def generate_key(self, return_key=False):
        """Randomly generate a ship placement answer key.

        Save the key as a Numpy np.ndarray, where each element represents one cell of the game grid. Indicate an empty
        cell by the value 0, and indicate a ship cell by adding the value 1. Place each ship at a random position on the
        board, and in a random orientation, horizontal or vertical. Continue to generate random configurations until
        all ships are placed without overlap, i.e., no cell has a value greater than 1.

        Returns
        -------
        np.ndarray
            Numpy array with shape self.board_shape, where 1 indicates a ship cell and -1 indicates an empty cell.
        """
        # Initialize the key
        self.key = 2 * np.ones(self.board_shape, dtype=np.int32)

        # Loop until a valid configuration is found
        # If none of the ships overlap, then we have a valid configuration
        while np.any(self.key > 1):
            # Reset the key
            self.key *= 0

            # Place each ship
            for s in self.ships:
                # Flip a fair coin to determine horizontal or vertical ship placement
                f = np.random.randint(0, 2)
                p = (s - 1) * f
                q = (s - 1) * (1 - f)

                # Randomly pick a place on the board which can fit the ship
                x = np.random.randint(0, self.board_shape[0] - p)
                y = np.random.randint(0, self.board_shape[1] - q)

                # Update the key with the ship placement
                self.key[x:(x + p + 1), y:(y + q + 1)] += 1

        self.key *= 2
        self.key -= 1

        if return_key:
            return self.key

    def reset(self):
        """Reset the game and generate a new key."""
        self.state *= 0
        self.n_hits = 0
        self.n_misses = 0
        self.n_repeats = 0
        self.n_turns = 0
        self.game_over = False
        self.generate_key()

    def step(self, model=None, eps_action=0.05, eps_min=0.01, use_qtable=False):
        """Take a single turn in the Battleship game.

        Parameters
        ----------
        model : Keras Sequential model
            A feedforward network which takes the board state as input and outputs a Q value prediction for each action.
        eps_action : float
            Threshold for choosing a random action.
        eps_min : float
            Minimum threshold for choosing a random action.

        Returns
        -------
        (np.ndarray, float, float, np.ndarray)
            A tuple containing the current state, the chosen action, the resulting reward, and the following state.
        """
        # Choose an action, to attack any of the cells
        # This is a linear index in the range 0..n_cells-1
        # Sometimes the action is random
        if (np.random.random() < np.maximum(eps_action, eps_min)) or (model is None):
            action_idx = np.random.randint(0, self.n_cells)
        elif use_qtable:
            flat_state = np.nonzero((self.state + 1).flatten())[0]
            board_idx = np.sum(3**flat_state * (self.state + 1).flatten()[flat_state])
            action_idx = np.argmax(model[board_idx])
        else:
            # Given the current state, what Q value does the model predict for each action
            # Input a batch with one element and a single channel
            # Select the first (and only) element of the batch with index [0] of the model output
            # Chose the action corresponding to the largest Q value
            action_idx = np.argmax(model.predict(np.asarray([[self.state]]))[0])

        # Map the linear index to an (x,y) tuple representing the cell position on the board
        action = self.action_map[action_idx]

        # Simple discrete reward function
        # Encourage hits, discourage misses, strongly discourage repeat actions
        if self.state[action] != 0:
            # Repeat action
            reward = -5
            self.n_repeats += 1
        elif self.key[action] == 1:
            # Got a hit
            # reward = -np.log(self.max_hits - self.n_hits) + np.log(self.n_cells)
            reward = 1
            self.n_hits += 1
            # Hit every cell of every ship?
            if self.n_hits == self.max_hits:
                self.game_over = True
                reward += 10
        else:
            # Got a miss
            # reward = -np.log(self.n_cells - self.max_hits - self.n_misses) + np.log(self.n_cells)
            self.n_misses += 1
            reward = -1

        # Deep copy
        in_state = np.copy(self.state)

        # Update cell: hit or miss
        self.state[action] = self.key[action]

        # Take a turn
        self.n_turns += 1

        # Add this turn to the memory queue
        return in_state, action_idx, reward, self.state, self.game_over
